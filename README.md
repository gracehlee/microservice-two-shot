# Wardrobify

Team:

* Grace Lee - Shoes 👟
* Judah Viggers - Hats 🧢

## Design
Most of the design elements were pulled from Bootstrap elements
and modified for our purposes. The design elements should be
very similar for what we used in Conference-GO, with minor tweaks.

## Shoes microservice

I created an inner navigation specifically for Shoes so
it wouldn't affect the main Navigation. There are two models,
Shoe and BinVO, in my shoes app. The BinVO instances are created
through the poller.py, which pulls the back-end database for
Bin from the Wardrobe API.

Within the ghi folder for the shoes app, there are a few JS forms:
1. ShoeNav: inner navigation for shoes page
2. ShoeList: lists all the shoes in a table format
3. ShoeForm: form to create a shoe instance
4. ShoeDelete: lists all the shoes and has a button to delete a specific shoe.

These pages are interconnected through the App.js and BrowserRouter.


## Hats microservice

The Hats navigation involves "Hats" and "Create" in the main navigation.
Under "Hats", you can view the list of Hat instances in two ways-
with a table list form and a card form. In the card, there is a delete
button that will delete the instance of the Hat, and an update button,
which has not been updated with functionality yet but is there for
proof of concept.
- The delete button will prompt the user to confirm whether they would
like to delete the item; upon pressing "OK", the item will be deleted.

Similarly to the shoes app, the hats app also has a poller.py that will
pull the back-end database for Location from the Wardrobe API.
The actual Hats app makes use of the Hat model and the LocationVO model,
which is a value object created through the poller.py.

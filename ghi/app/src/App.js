import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoesList from './ShoesList';
import ShoeForm from './ShoeForm';
import ShoeDelete from './ShoeDelete';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatList />} />
          <Route path="/hats/new" element={<HatForm />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path="/shoes/new" element={<ShoeForm />} />
          <Route path="/shoes/delete" element={<ShoeDelete />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

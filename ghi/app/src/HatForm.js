import React, {useEffect, useState} from 'react' ;


function HatForm() {


    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState ({
        fabric: '',
        style_name: '',
        color:'',
        url:'',
        location:'',
    })

    const getData = async () => {
        const url = 'http://localhost:8100/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const hat = {};
        hat.fabric = formData.fabric
        hat.style_name = formData.style_name
        hat.color = formData.color
        hat.url = formData.url
        hat.location = formData.location
        const url = `http://localhost:8090/api/hats/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(hat),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                fabric: '',
                style_name: '',
                color: '',
                url:'',
                location: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new Hat</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.fabric} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control"/>
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.style_name} placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control"/>
                            <label htmlFor="style_name">Style_name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} value={formData.url} placeholder="url" required type="url" name="url" id="url" className="form-control"/>
                            <label htmlFor="url">url</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleFormChange} value={formData.location} required id="location" name="location" className="form-select">
                                <option value="">Location</option>
                                {locations.map(location => {
                                    return (
                                    <option value={location.id} key={location.href}>
                                        {location.closet_name}
                                    </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );


}
export default HatForm;
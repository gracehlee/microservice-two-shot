import React, { useEffect, useState } from "react";

import { Link } from 'react-router-dom';

function HatList() {
  const [hats, setHats] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8090/api/hats/";
    try {
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
      } else {
        throw new Error('Failed to fetch hats');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const deleteHat = async (hatId) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this hat?");
    if (!confirmDelete) {
      return;
    }

    try {
      const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, {
        method: 'DELETE',
      });

      if (response.ok) {
        setHats(hats.filter(hat => hat.id !== hatId));
      } else {
        throw new Error('Failed to delete hat');
      }
    } catch (error) {
      console.error('Error deleting hat:', error);
    }
  };

  return (
    <>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Picture</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => (
            <tr key={hat.id}>
              <td>{hat.fabric}</td>
              <td>{hat.style_name}</td>
              <td>{hat.color}</td>
              <td><img src={hat.URL} width="50" height="40" alt="Hat"/></td>
              <td>{hat.location.closet_name} - {hat.location.section_number}</td>
            </tr>
          ))}
        </tbody>
      </table>

      <div className="row">
        {hats.map(hat => (
          <div key={hat.id} className="card w-25 m-3 mb-3 shadow">
            <img width="200" className="card-img-top bg-white rounded shadow d-block mx-auto mb-4" src={hat.picture_url} alt="Hat" />
            <div className="card-body">
              <h5 className="card-title">{hat.fabric}</h5>
              <h6 className="card-title">{hat.style_name}</h6>
              <p className="card-text">Closet: {hat.location.closet_name}</p>
              <p className="card-text">Location: {hat.location.section_number}</p>
              <Link to={`/hats/update/${hat.id}/`}>
                <button className="btn btn-primary">Update</button>
              </Link>
              <button className="btn btn-danger" onClick={() => deleteHat(hat.id)}>
                Delete
              </button>
            </div>
          </div>
        ))}
      </div>
    </>
  );
}

export default HatList;
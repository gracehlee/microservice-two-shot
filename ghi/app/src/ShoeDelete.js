import React, { useEffect, useState } from "react";
import ShoeNav from './ShoeNav';


function ShoeDelete() {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
            console.log(data);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (shoe_id) => {
        const deleteUrl = `http://localhost:8080/api/shoes/${shoe_id}`;
        await fetch(deleteUrl, { method: "delete" });
        fetchData();
    }

    const columns = [[], [], []];
    let index = 0;
    for (let shoe of shoes) {
        columns[index].push(shoe);
        index++;
        if (index > 2) {
            index = 0;
        }
    }

    function renderShoe(shoe) {
        return (
            <div key={shoe.id} className="card mb-3 shadow">
                <img src={shoe.picture_url} className="card-img-top" width="100" height="300" alt=""/>
                <div className="card-body">
                    <h5 className="card-title">{shoe.model_name}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">
                        <span>{shoe.manufacturer}</span>
                    </h6>
                    <p className="card-text">
                        <span className="text-muted" style={{fontWeight: 'bold'}}>Color: </span>{shoe.color}<br></br>
                        <span className="text-muted" style={{fontWeight: 'bold'}}>ID: </span>{shoe.id}
                    </p>
                </div>
                <div className="card-footer" style={{display: 'flex', justifyContent: 'space-between'}}>
                    <span>
                        { shoe.bin.closet_name } - Bin: { shoe.bin.bin_number }
                    </span>
                    <span>
                        <button onClick={() => handleDelete(shoe.id)}>Delete</button>
                    </span>
                </div>
            </div>
        );
    }

    return (
        <>
            <ShoeNav />
                <div style={{ display: "grid", gridTemplateColumns: "repeat(3, 1fr)", gridGap: 20 }}>
                    <div className="col column-0">
                        {columns[0].map(shoe => renderShoe(shoe))}
                    </div>
                    <div className="col column-1">
                        {columns[1].map(shoe => renderShoe(shoe))}
                    </div>
                    <div>
                        {columns[2].map(shoe => renderShoe(shoe))}
                    </div>
                </div>
        </>
    )
}

export default ShoeDelete;

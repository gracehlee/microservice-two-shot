import React, { useEffect, useState } from 'react';
import ShoeNav from './ShoeNav';


function ShoeForm () {
        const [bins, setBins] = useState([]);

        const fetchData = async () => {
            const url = 'http://localhost:8100/api/bins/';

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setBins(data.bins);
            }
        };

        useEffect(() => {
            fetchData();
        }, []);

        const handleSubmit = async (event) => {
            event.preventDefault();

            const data = {};
            data.model_name = formData.model_name;
            data.manufacturer = formData.manufacturer;
            data.color = formData.color;
            data.picture_url = formData.picture_url;
            data.bin = formData.bin;

            console.log(data);

            const shoeUrl = 'http://localhost:8080/api/shoes/';
            const fetchConfig = {
                method: "post",
                body: JSON.stringify(data),
                headers: {
                'Content-Type': 'application/json',
                },
            };

            const response = await fetch(shoeUrl, fetchConfig);
            if (response.ok) {
                const newShoe = await response.json();
                console.log(newShoe);

                setFormData({
                    model_name: '',
                    manufacturer: '',
                    color: '',
                    picture_url: '',
                    bin: '',
                });
            };
        };

        const [formData, setFormData] = useState({
            model_name: '',
            manufacturer: '',
            color: '',
            picture_url: '',
            bin: '',
        });

        const handleFormInput = (event) => {
            setFormData({
                ...formData,
                [event.target.name]: event.target.value,
            })
        };

        const handleNameChange = handleFormInput;
        const handleManufacturerChange = handleFormInput;
        const handleColorChange = handleFormInput;
        const handlePictureChange = handleFormInput;
        const handleBinChange = handleFormInput;

        return (
            <>
                <ShoeNav />
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1 style={{display: 'grid', justifyContent: 'center'}}>Create a new shoe</h1>
                            <form onSubmit={handleSubmit} id="create-shoe-form" >
                                <div className="form-floating mb-3">
                                    <input onChange={handleNameChange} value={formData.model_name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                                    <label htmlFor="modelName">Model Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleManufacturerChange} value={formData.manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                                    <label htmlFor="manufacturer">Manufacturer</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleColorChange} value={formData.color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                                    <label htmlFor="color">Color</label>
                                </div>
                                <div className="mb-3">
                                    <input onChange={handlePictureChange} value={formData.picture_url} placeholder="Picture Url" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                                </div>
                                <div className="mb-3">
                                    <select onChange={handleBinChange} value={formData.bin} required id="bin" name="bin" className="form-select">
                                        <option value="">Choose a Bin</option>
                                        {bins.map(bin => {
                                            return (
                                            <option value={bin.id} key={bin.id}>
                                                {bin.closet_name} - {bin.bin_number}
                                            </option>
                                            );
                                        })}
                                    </select>
                                </div>
                                <div style={{display: 'grid', justifyContent: 'center'}}>
                                    <button className="btn btn-primary">Create</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </>
        );
};

export default ShoeForm;

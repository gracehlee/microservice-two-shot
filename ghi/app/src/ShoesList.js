import React, { useEffect, useState } from "react";
import ShoeNav from './ShoeNav';


function ShoesList() {
    const [shoes, setShoes] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
            console.log(data);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
      <>
        <ShoeNav />
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Id</th>
                <th>Model Name</th>
                <th>Manufacturer</th>
                <th>Color</th>
                <th>Picture URL</th>
                <th>Bin</th>
              </tr>
            </thead>
            <tbody>
              {shoes.map(shoe => {
                return (
                  <tr key={shoe.id}>
                    <td>{ shoe.id }</td>
                    <td>{ shoe.model_name }</td>
                    <td>{ shoe.manufacturer }</td>
                    <td>{ shoe.color }</td>
                    <td><img src={ shoe.picture_url } width="50" height="40" alt=""/></td>
                    <td>{ shoe.bin.closet_name } - Bin: { shoe.bin.bin_number }</td>
                  </tr>
                );
              })}
            </tbody>
        </table>
      </>
    )
}

export default ShoesList;

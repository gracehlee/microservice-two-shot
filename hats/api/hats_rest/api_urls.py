from .api_views import api_show_hat, api_hats_list
from django.urls import path


urlpatterns = [
    path("hats/<int:pk>/", api_show_hat, name='api_show_hat'),
    path('hats/', api_hats_list, name='api_hats_list'),
]
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json
# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
        "section_number",
        "shelf_number",
        "id",

    ]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "url",
        "location",
        "id",

    ]
    encoders = {
        "location": LocationVOEncoder()
    }

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder()
    }
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False
        )

    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()

        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

    hat.objects.filter(id=pk).update(**content)
    shoe = hat.objects.get(id=pk)
    return JsonResponse(
        shoe,
        encoder=HatDetailEncoder,
        safe=False,
    )

@require_http_methods(["GET", "POST"])
def api_hats_list(request):
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatDetailEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )
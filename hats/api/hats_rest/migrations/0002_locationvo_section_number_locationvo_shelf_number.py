# Generated by Django 4.0.3 on 2024-03-14 19:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='locationvo',
            name='section_number',
            field=models.PositiveSmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='locationvo',
            name='shelf_number',
            field=models.PositiveSmallIntegerField(default=0),
        ),
    ]
